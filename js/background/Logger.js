"use strict";

export class Logger {
	constructor(env, trader) {
		this.env = env;
		this.trader = trader;
	}
	info() {
		this.log('info', arguments);
	}
	error() {
		this.log('error', arguments);
	}
	log(level, args) {
		const output = this.env === 'production' ? this.trader : console;

		output[level](...args);
	}
}