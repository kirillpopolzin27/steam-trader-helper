import { SteamTradeOfferState } from './SteamTradeOfferState.js';
import { RateLimitedPromise } from './RateLimitedPromise.js';
import { Utils } from './Utils.js';

const throttle1s = RateLimitedPromise(1000);

export class SteamAPI {
	constructor(logger) {
		this.logger = logger;
		this.accountIsAuthed = false;
		this.accountSteamid = null;
		this.accountApiKey = null;
	}
	async tryGetAuthedSteamid() 
	{
		const steamid = null;
		const response = await fetchRateLimited('https://steamcommunity.com');
		const text = await response.text(); 
		const matches = text.match(/g_steamID = "([0-9]+)";/);
		
		if(matches != null) {
			this.accountIsAuthed = true;
			this.accountSteamid = matches[1];
		} else {
			this.accountIsAuthed = false;
			this.accountSteamid = null;
		}
		return this.accountSteamid;
	}
	async fetchApiKey()
	{
		const response = await fetchRateLimited('https://steamcommunity.com/dev/apikey');
		const key = await this._parseApiKey(response);
		if(this.accountApiKey !== key) {
			this.accountApiKey = key;
		}

		return this.accountApiKey;
	}
	async getOrFetchApiKey()
	{
		if(this.accountApiKey == null) {
			return await this.fetchApiKey();
		}
		return this.accountApiKey;
	}
	async _parseApiKey(response) {
		const text = await response.text();
		const matches = text.match(/<p>.+: ([A-Z0-9]+)<\/p>/);

		return matches[1];
	}
	async isApiKeyRegistered() 
	{
		await this.fetchApiKey();

		return this.accountApiKey != null;
	}

	async isAccountAuthorized() 
	{
		const steamid = await this.tryGetAuthedSteamid();
		return steamid != null;
	}
	async createTradeOffer(data) 
	{	
		const url = 'https://steamcommunity.com/tradeoffer/new/send',
			sessionidCookie = await Utils.getCookie('sessionid'),
			params = {
				sessionid: sessionidCookie.value,
				serverid: 1,
				partner: data.partnerSteamid,
				json_tradeoffer: data.jsonTradeOffer,
				trade_offer_create_params: data.tradeOfferCreateParams,
				tradeoffermessage: data.tradeOfferMessage
			}

		try 
		{
			const fetchQueuePriority = 0;
			const headers = {
				"X-Referer": data.partnerTradeUrl, // X-Referer заменяется на Referer в ExtensionManager.js
				"X-Origin": 'https://steamcommunity.com' // X-Origin заменяется на Origin в ExtensionManager.js
			};
			const response = await postForm(url, params, headers, fetchQueuePriority);
			const json = await response.json();

			return json;
		} 
		catch(e) 
		{
			this.logger.error(e);
			return JSON.parse(e.message);
		}
	}
	async getTradeOffer(tradeOfferId) {
		const apiKey = await this.getOrFetchApiKey();
		const url = 'https://api.steampowered.com/IEconService/GetTradeOffer/v1/?tradeofferid='+tradeOfferId+'&language=en&key='+apiKey;
		const response = await fetchRateLimited(url);
		const json = await response.json();
		return json.response.offer;
	}
	async cancelTradeOffer(tradeOfferId) {
		const url = 'https://steamcommunity.com/tradeoffer/'+tradeOfferId+'/cancel',
			sessionidCookie = await Utils.getCookie('sessionid'),
			params = {
				sessionid: sessionidCookie.value,
			};
		
		const response = await postForm(url, params);

		return response.json();
	}
	async registerApiKey() 
	{
		const url = 'https://steamcommunity.com/dev/registerkey',
			sessionidCookie = await Utils.getCookie('sessionid'),
			params = {
				domain: 'localhost',
				agreeToTerms: 'agreed',
				sessionid: sessionidCookie.value,
				Submit: 'Зарегистрировать',
			};

		const response = await postForm(url, params);
		return this._parseApiKey(response);
	}
	async getActiveSentOffers() 
	{
		const key = this.accountApiKey;
		const url = 'https://api.steampowered.com/IEconService/GetTradeOffers/v1/?key='+key+'&get_sent_offers=1&active_only=1';
		const response = await fetchRateLimited(url);
		const json = await response.json();

		return json.response.trade_offers_sent || [];
	}
	async isOfferAlreadyCreated(inputAssetids) 
	{
		try {
			const activeSentOffers = await this.getActiveSentOffers();

			for(const offer of activeSentOffers) {
				for(const steamTradeItem of offer.items_to_give) {
					if(
						inputAssetids.includes(steamTradeItem.assetid)
						&&  
						SteamTradeOfferState.inActiveRange(offer.trade_offer_state)
					) {
						return true;
					}
				}
			}

			return false;
		} catch (err) {
			return true;
		}
	}
}

async function postForm(url, params, headers = {}, fetchQueuePriority = 1) {
	const fd = new FormData();
	for(const key in params) fd.set(key, params[key]);
	const body = urlencodeFormData(fd)
	const response = await fetchRateLimited(url, {
		method: 'POST',
		headers: Object.assign(headers,{
			'Content-Type': 'application/x-www-form-urlencoded'
		}),
		body
	}, fetchQueuePriority);
	if(!response.ok) {
		const text = await response.text();
		this.logger.error(`url: ${url}, params: ${JSON.stringify(params)}, headers: ${JSON.stringify(headers)}, response: ${JSON.stringify(response)}, text: ${text}`);
		throw Error(text);
	}
	return response;
}

function urlencodeFormData(fd) {
    let s = '';
    function encode(s){ return encodeURIComponent(s).replace(/%20/g,'+'); }
    for(const pair of fd.entries()){
        if(typeof pair[1]=='string'){
            s += (s?'&':'') + encode(pair[0])+'='+encode(pair[1]);
        }
    }
    return s;
}

function fetchRateLimited(url, params = null, queuePriority = 1) {
	return throttle1s(queuePriority).then(() => fetch(url, params));
}
